# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v0.2.4 - 2024-06-11(10:16:55 +0000)

### Other

- Sharing network namespace not possible anymore

## Release v0.2.3 - 2024-06-04(14:26:25 +0000)

## Release v0.2.2 - 2024-01-12(10:02:52 +0000)

### Other

- UDP portforwarding rules are not applied

## Release v0.2.1 - 2023-12-20(14:59:46 +0000)

### Other

- only the first portforwarding entry of a container is set

## Release v0.2.0 - 2023-12-15(13:08:14 +0000)

### Other

- Service discovery - Advertisement via DNS.SD

## Release v0.1.7 - 2023-12-08(16:02:30 +0000)

### Other

- Network config only applied when access interface available

## Release v0.1.6 - 2023-11-06(10:09:36 +0000)

### Other

- - remove network access when no or invalid networkconfig is provided

## Release v0.1.5 - 2023-11-06(09:52:41 +0000)

### Fixes

- All amxm destructors should let top application unloading them

## Release v0.1.4 - 2023-11-06(09:04:36 +0000)

### Other

- - resolve path if rule-path contains searchpath

## Release v0.1.3 - 2023-10-27(06:55:00 +0000)

### Other

- Cthulhu crash on wrong interface

## Release v0.1.2 - 2023-10-17(13:19:39 +0000)

### Other

- default NetworkConfig need update

## Release v0.1.1 - 2023-10-04(13:33:44 +0000)

### Other

- Opensource component

## Release v0.1.0 - 2023-09-28(10:15:48 +0000)

### Other

- Add the possibility to share the host network namespace with the container

## Release v0.0.4 - 2023-09-01(15:19:39 +0000)

### Other

- resolve crash when interface does not exist

## Release v0.0.3 - 2023-06-29(15:53:45 +0000)

### Other

- Modify NetworkConfig

## Release v0.0.2 - 2023-06-16(14:25:43 +0000)

## Release v0.0.3 - 2023-05-08(12:27:20 +0000)

### Other

- replace bash with sh

## Release v0.0.2 - 2022-12-23(15:09:19 +0000)

