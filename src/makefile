include ../makefile.inc

# build destination directories
OBJDIR = ../output/$(MACHINE)

# TARGETS
TARGET_SO = $(OBJDIR)/$(COMPONENT).so

# directories
# source directories
INCDIRS = $(INCDIR) $(INCDIR_PRIV)

SRCDIR = $(shell pwd)
INCDIR_PRIV = ../include_priv
INCDIRS += $(if $(STAGINGDIR), $(STAGINGDIR)/include)
STAGINGLIBDIR = -L$(STAGINGDIR)/lib -L$(STAGINGDIR)/usr/lib

# files
SOURCES = $(wildcard $(SRCDIR)/*.c)
OBJECTS = $(addprefix $(OBJDIR)/,$(notdir $(SOURCES:.c=.o)))

# compilation and linking flags
CFLAGS += -Werror -Wall -Wextra \
		-Wformat=2 -Wshadow \
		-Wwrite-strings -Wredundant-decls \
		-Wmissing-declarations -Wno-attributes \
		-Wno-format-nonliteral \
		-DSAHTRACES_ENABLED -DSAHTRACES_LEVEL=500 \
		-fPIC -g3 $(addprefix -I ,$(INCDIRS))

ifeq ($(CC_NAME),g++)
	CFLAGS += -std=c++2a
else
	CFLAGS += -Wstrict-prototypes -Wold-style-definition -Wnested-externs -std=c11
endif

LDFLAGS += $(STAGINGLIBDIR) \
		-shared -fPIC \
		-lamxc -lamxm \
		-lamxp -lamxd \
		-lamxo -lamxb \
		-lsahtrace \
		-lcthulhu -llcm

# targets
all: $(TARGET_SO)

$(TARGET_SO): $(OBJECTS)
	@echo $(STAGINGLIBDIR)
	$(CC) -Wl,-soname,$(COMPONENT).so -o $@ $(OBJECTS) $(LDFLAGS)


-include $(OBJECTS:.o=.d)

$(OBJDIR)/%.o: $(SRCDIR)/%.c | $(OBJDIR)/
	$(CC) $(CFLAGS) -c -o $@ $<
	@$(CC) $(CFLAGS) -MM -MP -MT '$(@) $(@:.o=.d)' -MF $(@:.o=.d) $(<)

$(OBJDIR)/:
	$(MKDIR) -p $@

clean:
	rm -rf ../output/

.PHONY: clean
