/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#define _GNU_SOURCE 1 // to make strdup work

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <amxc/amxc.h>
#include <amxm/amxm.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <amxb/amxb.h>
#include <amxo/amxo.h>
#include <amxc/amxc_macros.h>

#include <cthulhu/cthulhu.h>
#include <cthulhu/cthulhu_defines.h>
#include <cthulhu/cthulhu_defines_plugin.h>
#include <cthulhu/cthulhu_helpers.h>
#include <lcm/lcm_assert.h>
#include <debug/sahtrace.h>


#include "cthulhu_networking.h"
#include "cthulhu_networking_defines.h"

#define ME "networking"

#define GET_UINT16(a, n) amxc_var_dyncast(uint16_t, n == NULL ? a : GET_ARG(a, n))

static amxm_shared_object_t* so = NULL;
static amxm_module_t* mod = NULL;
static amxd_dm_t* dm = NULL;
static amxb_bus_ctx_t* amxb_bus_ctx_cthulhu = NULL;

#define PORTMAPPING_PATH "Device.NAT.PortMapping."
#define DNSSD_PATH "Device.DNS.SD.Advertise."

typedef enum _networking_ns_type {
    ns_type_empty,
    ns_type_veth,
    ns_type_parent,
} networking_ns_type_t;

static inline amxb_bus_ctx_t* networking_get_dnssd_bus_ctx(void) {
    static bool querried = false;
    static amxb_bus_ctx_t* device_ctx = NULL;

    if(querried) {
        return device_ctx;
    }

    device_ctx = amxb_be_who_has(DNSSD_PATH);
    if(!device_ctx) {
        SAH_TRACEZ_WARNING(ME, "Device context for %s not found", DNSSD_PATH);
    }
    querried = true;
    return device_ctx;
}

static inline amxb_bus_ctx_t* networking_get_portforwarding_bus_ctx(void) {
    static bool querried = false;
    static amxb_bus_ctx_t* device_ctx = NULL;

    if(querried) {
        return device_ctx;
    }

    device_ctx = amxb_be_who_has(PORTMAPPING_PATH);
    if(!device_ctx) {
        SAH_TRACEZ_WARNING(ME, "Device context for %s not found", PORTMAPPING_PATH);
    }
    querried = true;
    return device_ctx;
}

static amxd_object_t* networking_sb_get(const char* const sb_id) {
    return amxd_dm_findf(dm, CTHULHU_DM_SANDBOX_INSTANCES ".[" CTHULHU_SANDBOX_ID " == '%s'].", sb_id);
}

static amxd_object_t* networking_ctr_get(const char* const ctr_id) {
    return amxd_dm_findf(dm, CTHULHU_DM_CONTAINER_INSTANCES ".[" CTHULHU_CONTAINER_ID " == '%s'].", ctr_id);
}

static inline amxd_object_t* networking_get_plugin_settings(amxd_object_t* obj) {
    return amxd_object_findf(obj, CTHULHU_PLUGINS_PRIVATE "." NC ".");
}

/**
 * @brief Get the firewall rules path object of the default chain
 *
 * @return char* path to the firewall chain rules object. this should be freed.
 */
static char* networking_get_firewall_chain_rules(void) {
    char* firewall_chain_rules = NULL;
    amxd_object_t* cthulhu = NULL;
    amxd_object_t* plugin_settings = NULL;
    char* firewall_chain = NULL;

    cthulhu = amxd_dm_findf(dm, CTHULHU);
    when_null_log(cthulhu, exit);
    plugin_settings = networking_get_plugin_settings(cthulhu);
    when_null_log(plugin_settings, exit);
    firewall_chain = amxd_object_get_cstring_t(plugin_settings, NC_DEFAULTFIREWALLCHAIN, NULL);
    when_str_empty_log(firewall_chain, exit);
    if(asprintf(&firewall_chain_rules, "%sRule.", firewall_chain) < 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to allocate string");
        goto exit;
    }
exit:
    free(firewall_chain);
    return firewall_chain_rules;
}

static inline char* networking_find_interface_path(const char* const interface) {
    amxd_object_t* itf_obj = amxd_dm_findf(dm, CTHULHU "." CTHULHU_PLUGINS_PRIVATE "." NC "."NC_INTERFACES ".[" NC_INTERFACES_NAME " == '%s'].", interface);
    if(!itf_obj) {
        return NULL;
    }
    return amxd_object_get_cstring_t(itf_obj, NC_INTERFACES_REFERENCE, NULL);
}

/**
 * @brief translate an interface idfentifier to the real interface
 *
 * the interface can either be defined as an entry in the AccessInterfaces, or as a
 * logical interface entry in 'Device.Logical.'
 *
 *
 * @param interface
 * @return char* the real interface. This should be freed after use.
 */
static char* networking_get_real_interface(const char* const interface) {
    char* real_itf = NULL;
    char* tmp_itf = NULL;
    const char* to_compare = NULL;
    amxc_var_t values;
    amxc_var_init(&values);

    tmp_itf = networking_find_interface_path(interface);
    to_compare = tmp_itf? tmp_itf: interface;

    static const char* compare_path = "Device.Logical.";
    if(strncmp(compare_path, to_compare, strlen(compare_path)) != 0) {
        // interface does not start with Device.Logical. Return a copy of the original interface
        real_itf = strdup(to_compare);
        goto exit;
    }
    amxb_bus_ctx_t* device_ctx = NULL;
    device_ctx = amxb_be_who_has(to_compare);
    if(!device_ctx) {
        SAH_TRACEZ_ERROR(ME, "path [%s] not found on datamodel", to_compare);
        goto exit;
    }
    int res = amxb_get(device_ctx, to_compare, 1, &values, 5);
    if(res != 0) {
        SAH_TRACEZ_ERROR(ME, "amxb_get of %s failed with error %d", to_compare, res);
        goto exit;
    }
    amxc_var_t* itf_data = amxc_var_get_first(&values);
    when_null_log(itf_data, exit);
    amxc_var_t* dm_itf = GET_ARG(itf_data, to_compare);
    when_null_log(dm_itf, exit);
    amxc_var_t* lowerlayers_arg = GET_ARG(dm_itf, "LowerLayers");
    when_null_log(lowerlayers_arg, exit);
    real_itf = amxc_var_get_cstring_t(lowerlayers_arg);
    when_str_empty_log(real_itf, exit);

exit:
    free(tmp_itf);
    amxc_var_clean(&values);
    return real_itf;
}


static int networking_comp_strings(amxc_llist_it_t* it1, amxc_llist_it_t* it2) {
    amxc_string_t* s1 = amxc_llist_it_get_data(it1, amxc_string_t, it);
    amxc_string_t* s2 = amxc_llist_it_get_data(it2, amxc_string_t, it);

    return strcmp(s1->buffer, s2->buffer);
}

/**
 * @brief compare two llists
 *
 * the lists need to be sorted before calling them
 *
 * @param l1
 * @param l2
 * @param cmp
 * @return true
 * @return false
 */
static bool amxc_llist_equal(const amxc_llist_t* l1, const amxc_llist_t* l2, amxc_llist_it_cmp_t cmp) {
    bool res = false;
    when_null(l1, exit);
    when_null(l2, exit);
    when_null(cmp, exit);
    if(amxc_llist_size(l1) != amxc_llist_size(l2)) {
        goto exit;
    }
    if(amxc_llist_is_empty(l1)) {
        // both lists are empty
        res = true;
        goto exit;
    }
    amxc_llist_it_t* it1 = amxc_llist_get_first(l1);
    amxc_llist_it_t* it2 = amxc_llist_get_first(l2);
    for(; it1 && it2;) {
        if(cmp(it1, it2) != 0) {
            goto exit;
        }
        it1 = amxc_llist_it_get_next(it1);
        it2 = amxc_llist_it_get_next(it2);
    }
    res = true;

exit:
    return res;
}

static amxd_object_t* networking_get_priv_sb(amxd_object_t* ctr_obj) {
    amxd_object_t* priv_sb_obj = NULL;
    char* priv_sb_id = amxd_object_get_cstring_t(ctr_obj, CTHULHU_CONTAINER_PRIVATESANDBOX, NULL);
    when_str_empty_log(priv_sb_id, exit);
    priv_sb_obj = networking_sb_get(priv_sb_id);
    when_null_log(priv_sb_obj, exit);
exit:
    free(priv_sb_id);
    return priv_sb_obj;
}

static networking_ns_type_t networking_netns_get_type(amxd_object_t* ctr_obj) {
    networking_ns_type_t ns_type = ns_type_empty;
    char* ns_type_str = NULL;

    amxd_object_t* priv_sb_obj = networking_get_priv_sb(ctr_obj);
    when_null_log(priv_sb_obj, exit);
    amxd_object_t* netns_obj = amxd_object_get_child(priv_sb_obj, CTHULHU_SANDBOX_NETWORK_NS);
    when_null_log(netns_obj, exit);
    ns_type_str = amxd_object_get_cstring_t(netns_obj, CTHULHU_SANDBOX_NETWORK_NS_TYPE, NULL);
    when_null_log(ns_type_str, exit);
    if(strcmp(ns_type_str, CTHULHU_SANDBOX_NETWORK_NS_TYPE_EMPTY) == 0) {
        ns_type = ns_type_empty;
        goto exit;
    }
    if(strcmp(ns_type_str, CTHULHU_SANDBOX_NETWORK_NS_TYPE_VETH) == 0) {
        ns_type = ns_type_veth;
        goto exit;
    }
    if(strcmp(ns_type_str, CTHULHU_SANDBOX_NETWORK_NS_TYPE_PARENTSB) == 0) {
        ns_type = ns_type_parent;
        goto exit;
    }
exit:
    free(ns_type_str);
    return ns_type;
}

/**
 * @brief Configure the private sandbox network namespace to "VEth"
 *
 * @param sb_obj
 * @return int
 */
static int networking_netns_configure_veth(amxd_object_t* sb_obj, const char* interface, const char* bridge) {
    int res = -1;
    amxd_object_t* itf_obj = NULL;
    amxd_object_t* netns_obj = amxd_object_get_child(sb_obj, CTHULHU_SANDBOX_NETWORK_NS);
    when_null_log(netns_obj, exit);
    amxd_object_set_cstring_t(netns_obj, CTHULHU_SANDBOX_NETWORK_NS_TYPE, CTHULHU_SANDBOX_NETWORK_NS_TYPE_VETH);
    amxd_object_set_bool(netns_obj, CTHULHU_SANDBOX_NETWORK_NS_ENABLE, true);
    amxd_object_t* interfaces = amxd_object_get_child(netns_obj, CTHULHU_SANDBOX_NETWORK_NS_INTERFACES);
    when_null_log(interfaces, exit);
    amxc_var_t params;
    amxc_var_init(&params);
    amxc_var_set_type(&params, AMXC_VAR_ID_HTABLE);
    amxc_var_add_new_key_cstring_t(&params, CTHULHU_SANDBOX_NETWORK_NS_INTERFACES_BRIDGE, bridge);
    amxc_var_add_new_key_cstring_t(&params, CTHULHU_SANDBOX_NETWORK_NS_INTERFACES_INTERFACE, interface);
    amxd_object_add_instance(&itf_obj, interfaces, NULL, 0, &params);
    amxc_var_clean(&params);
    res = 0;
exit:
    return res;
}

/**
 * @brief Configure the private sandbox network namespace to "Empty" and remove all interfaces
 *
 * @param ctr_obj
 * @return int
 */
static int networking_netns_configure_empty(amxd_object_t* ctr_obj) {
    int res = -1;
    amxd_object_t* priv_sb_obj = networking_get_priv_sb(ctr_obj);
    when_null_log(priv_sb_obj, exit);
    amxd_object_t* netns_obj = amxd_object_get_child(priv_sb_obj, CTHULHU_SANDBOX_NETWORK_NS);
    when_null_log(netns_obj, exit);
    amxd_object_set_cstring_t(netns_obj, CTHULHU_SANDBOX_NETWORK_NS_TYPE, CTHULHU_SANDBOX_NETWORK_NS_TYPE_EMPTY);
    amxd_object_set_bool(netns_obj, CTHULHU_SANDBOX_NETWORK_NS_ENABLE, true);
    amxd_object_t* interfaces = amxd_object_get_child(netns_obj, CTHULHU_SANDBOX_NETWORK_NS_INTERFACES);
    amxd_object_for_each(instance, it, interfaces) {
        amxd_object_t* itf = amxc_llist_it_get_data(it, amxd_object_t, it);
        amxd_object_free(&itf);
    }

    res = 0;
exit:
    return res;
}

/**
 * @brief Configure the private sandbox to use the same network namespace as it's parent sandbox
 *
 * @param ctr_obj
 * @return int
 */
static int networking_netns_configure_parentNS(amxd_object_t* ctr_obj) {
    int res = -1;
    amxd_object_t* priv_sb_obj = networking_get_priv_sb(ctr_obj);
    when_null_log(priv_sb_obj, exit);
    amxd_object_t* netns_obj = amxd_object_get_child(priv_sb_obj, CTHULHU_SANDBOX_NETWORK_NS);
    when_null_log(netns_obj, exit);
    amxd_object_set_cstring_t(netns_obj, CTHULHU_SANDBOX_NETWORK_NS_TYPE, CTHULHU_SANDBOX_NETWORK_NS_TYPE_PARENTSB);
    amxd_object_set_bool(netns_obj, CTHULHU_SANDBOX_NETWORK_NS_ENABLE, true);
    amxd_object_t* interfaces = amxd_object_get_child(netns_obj, CTHULHU_SANDBOX_NETWORK_NS_INTERFACES);
    amxd_object_for_each(instance, it, interfaces) {
        amxd_object_t* itf = amxc_llist_it_get_data(it, amxd_object_t, it);
        amxd_object_free(&itf);
    }

    res = 0;
exit:
    return res;
}

/**
 * @brief Apply a portforwarding setting to Devices.NAT.Portforwarding
 *
 * @param pf_obj
 * @param itf_path
 * @param ip_address
 * @param ext_port
 * @param int_port
 * @param protocol
 * @return int
 */
static int networking_portforwarding_add_entry(const char* const interface,
                                               const char* const ip_address,
                                               uint32_t ext_port, uint32_t int_port,
                                               const char* protocol, char** path) {
    int res = -1;
    amxb_bus_ctx_t* device_ctx = NULL;
    amxc_var_t values;
    amxc_var_t ret;

    if(path) {
        *path = NULL;
    }

    amxc_var_init(&values);
    amxc_var_init(&ret);

    device_ctx = networking_get_portforwarding_bus_ctx();
    if(!device_ctx) {
        SAH_TRACEZ_ERROR(ME, "path [%s] not found on datamodel", PORTMAPPING_PATH);
        goto exit;
    }

    amxc_var_set_type(&values, AMXC_VAR_ID_HTABLE);
    amxc_var_add_new_key_bool(&values, "Enable", true);
    amxc_var_add_new_key_cstring_t(&values, "Interface", interface);
    amxc_var_add_new_key_uint32_t(&values, "ExternalPort", ext_port);
    amxc_var_add_new_key_cstring_t(&values, "InternalClient", ip_address);
    amxc_var_add_new_key_uint32_t(&values, "InternalPort", int_port);
    amxc_var_add_new_key_cstring_t(&values, "Protocol", protocol);
    amxc_var_add_new_key_cstring_t(&values, "Description", CTHULHU);

    if(amxb_add(device_ctx, PORTMAPPING_PATH, 0, NULL, &values, &ret, 5) != amxd_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Failed to add port mapping");
        goto exit;

    }
    if(path) {
        amxc_var_t* rule = amxc_var_get_first(&ret);
        if(!rule) {
            SAH_TRACEZ_ERROR(ME, "Return value does not contain an object");
            goto exit;
        }
        amxc_var_t* path_var = GET_ARG(rule, "path");
        when_null_log(path_var, exit);
        *path = amxc_var_get_cstring_t(path_var);
    }

    res = 0;
exit:
    amxc_var_clean(&values);
    amxc_var_clean(&ret);
    return res;
}

/**
 * @brief Apply a portforwarding setting to Devices.NAT.Portforwarding
 *
 * @param pf_obj
 * @param itf_path
 * @param ip_address
 * @param ext_port
 * @param int_port
 * @param protocol
 * @return int
 */
static int networking_portforwarding_add_entry_pfobj(amxd_object_t* pf_obj, const char* const itf_path,
                                                     const char* const ip_address,
                                                     uint32_t ext_port, uint32_t int_port,
                                                     const char* protocol) {
    int res = -1;
    char* path = NULL;

    if(networking_portforwarding_add_entry(itf_path,
                                           ip_address, ext_port, int_port,
                                           protocol, &path) != 0) {
        amxd_object_set_cstring_t(pf_obj,
                                  NC_PORTFORWARDING_STATUS,
                                  NC_PORTFORWARDING_STATUS_ERROR);
        goto exit;
    }
    amxd_object_set_cstring_t(pf_obj,
                              NC_PORTFORWARDING_STATUS,
                              NC_PORTFORWARDING_STATUS_OK);
    amxd_object_set_cstring_t(pf_obj,
                              NC_PORTFORWARDING_PATH,
                              path);
    res = 0;
exit:
    free(path);
    return res;
}

/**
 * @brief Add a Firewall entry to the datamodel
 *
 * @param ctr_obj
 * @param path
 * @return int
 */
static int networking_firewall_add_entry_to_dm(amxd_object_t* ctr_obj, const char* const path) {
    int res = -1;
    amxd_trans_t trans;
    amxd_status_t status = amxd_status_ok;
    amxd_object_t* config = NULL;
    amxd_object_t* rule_instances = NULL;

    amxd_trans_init(&trans);
    config = networking_get_plugin_settings(ctr_obj);
    when_null_log(config, exit);
    rule_instances = amxd_object_findf(config, NC_FIREWALLRULES);
    when_null_log(rule_instances, exit);

    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_select_object(&trans, rule_instances);
    amxd_trans_add_inst(&trans, 0, NULL);
    amxd_trans_set_cstring_t(&trans, NC_FIREWALLRULES_PATH, path);
    status = amxd_trans_apply(&trans, dm);
    if(status != amxd_status_ok) {
        SAH_TRACEZ_NOTICE(ME, "Transaction failed. Firewall reference not added to DM. Status: %d", status);
        goto exit;
    }

    res = 0;
exit:
    amxd_trans_clean(&trans);
    return res;
}

/**
 * @brief Apply a firewall Rule to the Firewall
 *
 * @param ctr_obj
 * @param firewall_chain_rules
 * @param source_ip
 * @param dest_itf
 * @param target
 * @return int
 */
static int networking_firewall_add_entry(amxd_object_t* ctr_obj,
                                         const char* firewall_chain_rules,
                                         const char* const source_ip,
                                         const char* const dest_itf,
                                         const char* target) {
    int res = -1;
    amxc_var_t values;
    amxc_var_t ret;
    amxb_bus_ctx_t* device_ctx = NULL;

    amxc_var_init(&values);
    amxc_var_init(&ret);
    device_ctx = amxb_be_who_has(firewall_chain_rules);
    if(!device_ctx) {
        SAH_TRACEZ_WARNING(ME, "Device context for %s not found", firewall_chain_rules);
        res = 0;
        goto exit;
    }
    amxc_var_set_type(&values, AMXC_VAR_ID_HTABLE);
    amxc_var_add_new_key_bool(&values, "Enable", true);
    amxc_var_add_new_key_cstring_t(&values, "SourceIP", source_ip);
    amxc_var_add_new_key_cstring_t(&values, "DestInterface", dest_itf);
    amxc_var_add_new_key_cstring_t(&values, "Target", target);
    amxc_var_add_new_key_cstring_t(&values, "Description", CTHULHU);
    amxc_var_add_new_key_int32_t(&values, "IPVersion", 4);

    if(amxb_add(device_ctx, firewall_chain_rules, 0, NULL, &values, &ret, 5) != amxd_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Failed to add firewall rule");
        goto exit;
    }
    const char* path = NULL;
    amxc_var_t* var = &ret;
    while(!path) {
        var = amxc_var_get_first(var);
        if(!var) {
            break;
        }
        path = GET_CHAR(var, "path");
    }

    when_str_empty_log(path, exit);
    when_failed_log(networking_firewall_add_entry_to_dm(ctr_obj, path), exit);

    res = 0;
exit:
    amxc_var_clean(&values);
    amxc_var_clean(&ret);
    return res;
}

static int networking_portforwarding_apply_one(amxd_object_t* pf_obj, const char* const ip_address) {
    int res = -1;
    amxd_status_t get_status = amxd_status_ok;
    char* status = NULL;
    char* pf_interface = NULL;
    char* itf_path = NULL;
    char* protocol = NULL;
    status = amxd_object_get_cstring_t(pf_obj, NC_PORTFORWARDING_STATUS, NULL);
    when_null_log(status, exit);
    if(strcmp(status, NC_PORTFORWARDING_STATUS_OFF) != 0) {
        SAH_TRACEZ_ERROR(ME, "Portforwarding state is %s, do nothing", status);
        res = 0;
        goto exit;
    }
    protocol = amxd_object_get_cstring_t(pf_obj, NC_PORTFORWARDING_PROTOCOL, NULL);
    when_null_log(protocol, exit);
    pf_interface = amxd_object_get_cstring_t(pf_obj, NC_PORTFORWARDING_INTERFACE, NULL);
    when_null_log(pf_interface, exit);
    itf_path = networking_get_real_interface(pf_interface);
    when_null_log(itf_path, exit);
    uint16_t int_port = amxd_object_get_uint32_t(pf_obj, NC_PORTFORWARDING_INTERNALPORT, &get_status);
    when_failed_log(get_status, exit);
    uint16_t ext_port = amxd_object_get_uint32_t(pf_obj, NC_PORTFORWARDING_EXTERNALPORT, &get_status);
    when_failed_log(get_status, exit);

    res = networking_portforwarding_add_entry_pfobj(pf_obj, itf_path, ip_address, ext_port, int_port, protocol);
exit:
    free(status);
    free(pf_interface);
    free(itf_path);
    free(protocol);
    return res;
}

/**
 * @brief Check if there are any portforwarding rules associated with this interface
 *
 * @param interface
 * @param ip_address
 * @return int
 */
static int networking_portforwarding_apply(amxd_object_t* ctr_obj, const char* const ip_address) {
    int res = -1;
    amxd_object_t* pf_instances_obj = NULL;
    amxd_object_t* pf_obj = NULL;
    amxd_object_t* config = NULL;

    config = networking_get_plugin_settings(ctr_obj);
    when_null_log(config, exit);
    pf_instances_obj = amxd_object_findf(config, NC_PORTFORWARDING);
    when_null_log(pf_instances_obj, exit);

    amxd_object_for_each(instance, it_pf, pf_instances_obj) {
        pf_obj = amxc_container_of(it_pf, amxd_object_t, it);
        when_failed_log(networking_portforwarding_apply_one(pf_obj, ip_address), exit);
    }
    res = 0;
exit:
    return res;
}

/**
 * @brief check if the DNS.SD.Advertise template has an 'Interface' and 'Domain' parameter
 *
 * not all implementations of DNS.SD have the 'Interface' and 'Domain' parameter yet. this function
 * will test if the datamodel has the parameters
 *
 */
static void networking_dnssd_has_interface(amxb_bus_ctx_t* device_ctx, bool* has_interface, bool* has_domain) {
    static bool querried = false;
    static bool cache_has_interface = false;
    static bool cache_has_domain = false;
    // check the datamodel only once. If checked before, return the cached value.
    if(querried) {
        goto exit;
    }

    amxc_var_t ret;
    amxc_var_init(&ret);

    amxb_describe(device_ctx, DNSSD_PATH, AMXB_FLAG_PARAMETERS, &ret, 5);

    amxc_var_t* element = amxc_var_get_first(&ret);
    if(!element) {
        SAH_TRACEZ_NOTICE(ME, "Could not describe %s", DNSSD_PATH);
        goto exit;
    }
    cache_has_interface = (amxc_var_get_path(element, "parameters.Interface", AMXC_VAR_FLAG_DEFAULT) != NULL);
    cache_has_domain = (amxc_var_get_path(element, "parameters.Domain", AMXC_VAR_FLAG_DEFAULT) != NULL);
exit:
    querried = true;
    *has_interface = cache_has_interface;
    *has_domain = cache_has_domain;
}

/**
 * @brief add a DNSSD entry to Device.DNS.SD.Advertise.
 *
 * The instancename of the entry is set to "Cthulhu" to be able to identify it later
 *
 * if the path is not null, the path of the entry wil lbe filled in with the path of the datamodel
 *
 * @param enable
 * @param interface
 * @param instancename
 * @param application_protocol
 * @param transport_protocol
 * @param domain
 * @param port
 * @param path
 * @return int
 */
static int networking_dnssd_add_entry(const char* alias, bool enable, const char* interface, const char* instance_name,
                                      const char* application_protocol, const char* transport_protocol,
                                      const char* domain, uint16_t port, char** path) {
    int res = -1;
    amxb_bus_ctx_t* device_ctx = NULL;
    amxc_var_t values;
    amxc_var_t ret;

    if(path) {
        *path = NULL;
    }

    amxc_var_init(&values);
    amxc_var_init(&ret);

    device_ctx = networking_get_dnssd_bus_ctx();
    when_null_log(device_ctx, exit);

    bool has_interface = false;
    bool has_domain = false;
    networking_dnssd_has_interface(device_ctx, &has_interface, &has_domain);

    amxc_var_set_type(&values, AMXC_VAR_ID_HTABLE);
    amxc_var_add_new_key_cstring_t(&values, "Alias", alias);
    amxc_var_add_new_key_bool(&values, "Enable", enable);
    if(has_interface) {
        amxc_var_add_new_key_cstring_t(&values, "Interface", interface);
    }
    amxc_var_add_new_key_cstring_t(&values, "InstanceName", instance_name);
    amxc_var_add_new_key_cstring_t(&values, "ApplicationProtocol", application_protocol);
    amxc_var_add_new_key_cstring_t(&values, "TransportProtocol", transport_protocol);
    if(has_domain) {
        amxc_var_add_new_key_cstring_t(&values, "Domain", domain);
    }
    amxc_var_add_new_key_uint16_t(&values, "Port", port);

    if(amxb_add(device_ctx, DNSSD_PATH, 0, NULL, &values, &ret, 5) != amxd_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Failed to add DNSSD");
        goto exit;
    }
    if(path) {
        amxc_var_t* rule = amxc_var_get_first(&ret);
        if(!rule) {
            SAH_TRACEZ_ERROR(ME, "Return value does not contain an object");
            goto exit;
        }
        amxc_var_t* path_var = GET_ARG(rule, "path");
        when_null_log(path_var, exit);
        *path = amxc_var_get_cstring_t(path_var);
    }

    res = 0;
exit:
    amxc_var_clean(&values);
    amxc_var_clean(&ret);
    return res;
}

static int networking_dnssd_add_textrecord(const char* path, const char* key, const char* value) {
    int res = -1;
    amxb_bus_ctx_t* device_ctx = NULL;
    amxc_var_t values;
    amxc_var_t ret;

    amxc_var_init(&values);
    amxc_var_init(&ret);

    device_ctx = networking_get_dnssd_bus_ctx();
    when_null_log(device_ctx, exit);

    amxc_var_set_type(&values, AMXC_VAR_ID_HTABLE);

    amxc_var_add_new_key_cstring_t(&values, "Key", key);
    amxc_var_add_new_key_cstring_t(&values, "Value", value);

    if(amxb_add(device_ctx, path, 0, NULL, &values, &ret, 5) != amxd_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Failed to add textrecord");
        goto exit;
    }

    res = 0;
exit:
    amxc_var_clean(&values);
    amxc_var_clean(&ret);
    return res;
}

static int networking_dnssd_entry_enable(const char* path) {
    int res = -1;
    amxb_bus_ctx_t* device_ctx = NULL;
    amxc_var_t values;
    amxc_var_t ret;

    amxc_var_init(&values);
    amxc_var_init(&ret);

    device_ctx = networking_get_dnssd_bus_ctx();
    when_null_log(device_ctx, exit);

    amxc_var_set_type(&values, AMXC_VAR_ID_HTABLE);

    amxc_var_add_new_key_bool(&values, "Enable", true);

    if(amxb_set(device_ctx, path, &values, &ret, 5) < 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to enable dnssd");
        goto exit;
    }

    res = 0;
exit:
    amxc_var_clean(&values);
    amxc_var_clean(&ret);
    return res;
}

static int networking_dnssd_apply_one(amxd_object_t* ctr_obj, amxd_object_t* dnssd_obj,
                                      networking_ns_type_t ns_type, const char* ip_address) {
    int res = -1;
    amxd_status_t get_status = amxd_status_ok;
    char* status = NULL;
    bool enable = true;
    char* interface = NULL;
    char* real_itf = NULL;
    char* instance_name = NULL;
    char* application_protocol = NULL;
    char* transport_protocol = NULL;
    char* domain = NULL;
    char* target = NULL;
    char* path = NULL;
    amxc_string_t textrecord_path;
    amxc_string_t alias;
    amxd_trans_t trans;
    uint16_t port = 0;
    uint16_t internal_port = 0;
    amxd_object_t* textrecord_instances_obj = NULL;
    amxd_object_t* textrecord_obj = NULL;

    amxd_trans_init(&trans);
    amxc_string_init(&textrecord_path, 128);
    amxc_string_init(&alias, 128);

    status = amxd_object_get_cstring_t(dnssd_obj, NC_DNSSD_STATUS, NULL);
    when_null_log(status, exit);
    if(strcmp(status, NC_DNSSD_STATUS_OFF) != 0) {
        SAH_TRACEZ_INFO(ME, "DNSSD is already set");
        res = 0;
        goto exit;
    }
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_select_object(&trans, dnssd_obj);

    interface = amxd_object_get_cstring_t(dnssd_obj, NC_DNSSD_INTERFACE, NULL);
    when_null_log(interface, error);
    real_itf = networking_get_real_interface(interface);
    instance_name = amxd_object_get_cstring_t(dnssd_obj, NC_DNSSD_INSTANCENAME, NULL);
    when_null_log(instance_name, error);
    enable = amxd_object_get_bool(dnssd_obj, NC_DNSSD_ENABLE, &get_status);
    when_failed_log(get_status, error);
    application_protocol = amxd_object_get_cstring_t(dnssd_obj, NC_DNSSD_APPLICATIONPROTOCOL, NULL);
    when_null_log(application_protocol, error);
    transport_protocol = amxd_object_get_cstring_t(dnssd_obj, NC_DNSSD_TRANSPORTPROTOCOL, NULL);
    when_null_log(transport_protocol, error);
    domain = amxd_object_get_cstring_t(dnssd_obj, NC_DNSSD_DOMAIN, NULL);
    when_null_log(domain, error);
    port = amxd_object_get_uint32_t(dnssd_obj, NC_DNSSD_PORT, &get_status);
    when_failed_log(get_status, error);
    internal_port = amxd_object_get_uint32_t(dnssd_obj, NC_DNSSD_INTERNALPORT, &get_status);
    when_failed_log(get_status, error);

    amxc_string_appendf(&alias, "Cthulhu_%d_%d", amxd_object_get_index(ctr_obj), amxd_object_get_index(dnssd_obj));


    if(networking_dnssd_add_entry(alias.buffer, false, real_itf, instance_name,
                                  application_protocol, transport_protocol,
                                  domain, port, &path) != 0) {
        goto error;
    }
    amxd_trans_set_cstring_t(&trans, NC_DNSSD_STATUS, NC_DNSSD_STATUS_OK);
    amxd_trans_set_cstring_t(&trans, NC_DNSSD_PATH, path);

    textrecord_instances_obj = amxd_object_findf(dnssd_obj, NC_DNSSD_TEXTRECORD);
    when_null_log(textrecord_instances_obj, error);

    amxc_string_clean(&textrecord_path);
    amxc_string_appendf(&textrecord_path, "%sTextRecord", path);
    amxd_object_for_each(instance, it_textrecord, textrecord_instances_obj) {
        char* key = NULL;
        char* value = NULL;
        textrecord_obj = amxc_container_of(it_textrecord, amxd_object_t, it);

        key = amxd_object_get_cstring_t(textrecord_obj, NC_DNSSD_TEXTRECORD_KEY, NULL);
        when_null_log(key, error);
        value = amxd_object_get_cstring_t(textrecord_obj, NC_DNSSD_TEXTRECORD_VALUE, NULL);
        when_null_log(value, error);
        if(networking_dnssd_add_textrecord(textrecord_path.buffer, key, value) != 0) {
            SAH_TRACEZ_ERROR(ME, "Failed to add textrecord");
            goto error;
        }
    }

    if(enable) {
        networking_dnssd_entry_enable(path);
    }

    // port forwarding only makes sense in the VETH scenario
    // parent network usually means that the container has no network namespace
    // configured, so there is no need for port forwarding
    if(ns_type == ns_type_veth) {
        char* pf_path = NULL;
        if(networking_portforwarding_add_entry(real_itf, ip_address, port, internal_port,
                                               transport_protocol, &pf_path) != 0) {
            SAH_TRACEZ_ERROR(ME, "Failed to add portforwarding entry for %s", interface);
            goto error;
        }
        if(pf_path) {
            amxd_trans_set_cstring_t(&trans, NC_DNSSD_PORTFORWARDINGPATH, pf_path);
            free(pf_path);
        }
    }
    amxd_status_t t_status = amxd_trans_apply(&trans, dm);
    if(t_status != amxd_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Transaction failed: [%d]", t_status);
        goto error;
    }

    res = 0;
    goto exit;
error:
    // remove dnssd entry form Devices
    if(!string_is_empty(path)) {
        amxc_var_t ret;
        amxc_var_init(&ret);
        if(amxb_del(networking_get_dnssd_bus_ctx(), path, 0, NULL, &ret, 5) < 0) {
            SAH_TRACEZ_ERROR(ME, "Failed to remove dnssd %s", path);
        }
        amxc_var_clean(&ret);
    }
    // set entry to ERROR in datamodel
    amxd_trans_clean(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_select_object(&trans, dnssd_obj);
    amxd_trans_set_cstring_t(&trans,
                             NC_DNSSD_STATUS,
                             NC_DNSSD_STATUS_ERROR);
    amxd_trans_apply(&trans, dm);

exit:
    free(status);
    free(interface);
    free(real_itf);
    free(application_protocol);
    free(transport_protocol);
    free(domain);
    free(target);
    free(path);
    amxd_trans_clean(&trans);
    amxc_string_clean(&textrecord_path);
    amxc_string_clean(&alias);
    return res;
}


/**
 * @brief If a new IP address is detected, apply the DNSSD setting for the container
 *
 * @param ctr_obj
 * @param ip_address
 * @return int
 */
static int networking_dnssd_apply(amxd_object_t* ctr_obj, const char* const ip_address) {
    int res = -1;
    amxd_object_t* dnssd_instances_obj = NULL;
    amxd_object_t* dnssd_obj = NULL;
    amxd_object_t* config = NULL;

    networking_ns_type_t ns_type = networking_netns_get_type(ctr_obj);
    // if the network namespace is empty, dnssd should not be configured since
    // the network of the container is not accessible anyway
    if(ns_type == ns_type_empty) {
        SAH_TRACEZ_INFO(ME, "Network namespace is empty. No use to add DNSSD entries");
        res = 0;
        goto exit;
    }

    config = networking_get_plugin_settings(ctr_obj);
    when_null_log(config, exit);
    dnssd_instances_obj = amxd_object_findf(config, NC_DNSSD);
    when_null_log(dnssd_instances_obj, exit);

    amxd_object_for_each(instance, it_dnssd, dnssd_instances_obj) {
        dnssd_obj = amxc_container_of(it_dnssd, amxd_object_t, it);
        when_null_log(dnssd_obj, exit);
        when_failed(networking_dnssd_apply_one(ctr_obj, dnssd_obj, ns_type, ip_address), exit);
    }
exit:
    return res;
}

/**
 * @brief Get the access interfaces for a container
 *
 * @param ctr_obj
 * @return amxd_object_t*
 */
static amxc_llist_t* networking_get_access_itfs(amxd_object_t* ctr_obj) {
    amxc_llist_t* ctr_access_intfs = NULL;
    amxd_object_t* config = NULL;
    amxd_object_t* accessinterfaces = NULL;

    amxc_llist_new(&ctr_access_intfs);

    config = networking_get_plugin_settings(ctr_obj);
    when_null_log(config, exit);
    accessinterfaces = amxd_object_findf(config, NC_ACCESSINTERFACES);
    when_null_log(accessinterfaces, exit);
    amxd_object_for_each(instance, it, accessinterfaces) {
        amxd_object_t* itf = amxc_llist_it_get_data(it, amxd_object_t, it);
        char* ref = amxd_object_get_cstring_t(itf, NC_ACCESSINTERFACES_REFERENCE, NULL);
        if(ref) {
            amxc_llist_add_string(ctr_access_intfs, ref);
            free(ref);
        }
    }
exit:
    return ctr_access_intfs;
}

/**
 * @brief Get the firewall rules associated with a list of access interfaces
 *
 * @param access_itfs
 * @return amxd_object_t* rules. The object should not be freed
 */
static amxd_object_t* networking_get_firewall_rules(amxc_llist_t* access_itfs) {
    amxd_object_t* rules = NULL;
    amxd_object_t* cthulhu = amxd_dm_findf(dm, CTHULHU);
    when_null_log(cthulhu, exit);
    amxd_object_t* plugin_settings = networking_get_plugin_settings(cthulhu);
    when_null_log(plugin_settings, exit);
    amxc_llist_sort(access_itfs, networking_comp_strings);
    amxd_object_t* fw_rules = amxd_object_findf(plugin_settings, NC_FIREWALLRULES);
    when_null_log(fw_rules, exit);
    amxd_object_for_each(instance, it, fw_rules) {
        amxc_llist_t fw_rules_itfs;
        amxd_object_t* br_obj = amxc_llist_it_get_data(it, amxd_object_t, it);
        char* interfaces = amxd_object_get_cstring_t(br_obj, NC_FIREWALLRULES_INTERFACES, NULL);
        when_str_empty_log(interfaces, exit);
        amxc_string_t interfaces_str;
        amxc_string_init(&interfaces_str, 64);
        amxc_string_appendf(&interfaces_str, "%s", interfaces);

        amxc_llist_init(&fw_rules_itfs);
        if(amxc_string_split_to_llist(&interfaces_str, &fw_rules_itfs, ',') != AMXC_STRING_SPLIT_OK) {
            SAH_TRACEZ_ERROR(ME, "Could not split interfaces [%s]", interfaces_str.buffer);
            amxc_string_clean(&interfaces_str);
            amxc_llist_clean(&fw_rules_itfs, amxc_string_list_it_free);
            free(interfaces);
            goto exit;
        }
        amxc_llist_for_each(it_itfs, (&fw_rules_itfs)) {
            amxc_string_t* itf = amxc_llist_it_get_data(it_itfs, amxc_string_t, it);
            amxc_string_trim(itf, NULL);
        }
        amxc_llist_sort(&fw_rules_itfs, networking_comp_strings);
        if(amxc_llist_equal(access_itfs, &fw_rules_itfs, networking_comp_strings)) {
            // rules found
            rules = amxd_object_get(br_obj, NC_FIREWALLRULES_RULES);
            amxc_string_clean(&interfaces_str);
            amxc_llist_clean(&fw_rules_itfs, amxc_string_list_it_free);
            free(interfaces);
            goto exit;
        }
        amxc_string_clean(&interfaces_str);
        amxc_llist_clean(&fw_rules_itfs, amxc_string_list_it_free);
        free(interfaces);
    }

exit:
    return rules;
}

/**
 * @brief Apply the firewall rules for a container when a new IP address is detected
 *
 * @param ctr_obj
 * @param ip_address
 * @return int
 */
static int networking_firewall_apply(amxd_object_t* ctr_obj, const char* const ip_address) {
    int res = -1;
    amxc_llist_t* ctr_access_intfs = NULL;
    amxd_object_t* rules = NULL;

    char* firewall_chain_rules = networking_get_firewall_chain_rules();


    ctr_access_intfs = networking_get_access_itfs(ctr_obj);
    when_null(ctr_access_intfs, exit);
    if(amxc_llist_is_empty(ctr_access_intfs)) {
        SAH_TRACEZ_NOTICE(ME, "No access interfaces are defined");
        res = 0;
        goto exit;
    }
    rules = networking_get_firewall_rules(ctr_access_intfs);
    if(!rules) {
        SAH_TRACEZ_NOTICE(ME, "No firewall rules found");
        res = 0;
        goto exit;
    }
    amxd_object_for_each(instance, it, rules) {
        amxd_object_t* rule = amxc_llist_it_get_data(it, amxd_object_t, it);
        char* source_ip = amxd_object_get_cstring_t(rule, NC_FIREWALLRULES_RULES_SOURCEIP, NULL);
        char* dest_itf = amxd_object_get_cstring_t(rule, NC_FIREWALLRULES_RULES_DESTINTERFACE, NULL);
        char* target = amxd_object_get_cstring_t(rule, NC_FIREWALLRULES_RULES_TARGET, NULL);
        if(strcmp(source_ip, "CTR_IP") == 0) {
            networking_firewall_add_entry(ctr_obj, firewall_chain_rules, ip_address, dest_itf, target);
        } else {
            networking_firewall_add_entry(ctr_obj, firewall_chain_rules, source_ip, dest_itf, target);
        }
    }

    res = 0;
exit:
    free(firewall_chain_rules);
    amxc_llist_clean(ctr_access_intfs, amxc_string_list_it_free);
    return res;

}

/**
 * @brief Disable the portforwarding for a container
 *
 * @param ctr_obj
 * @return int
 */
static int networking_portforwarding_disable(amxd_object_t* ctr_obj) {
    int res = -1;
    amxd_object_t* pf_instances_obj = NULL;
    amxd_object_t* pf_obj = NULL;
    amxd_object_t* config = NULL;
    amxb_bus_ctx_t* device_ctx = NULL;

    config = networking_get_plugin_settings(ctr_obj);
    when_null_log(config, exit);
    pf_instances_obj = amxd_object_findf(config, NC_PORTFORWARDING);
    when_null_log(pf_instances_obj, exit);
    device_ctx = networking_get_portforwarding_bus_ctx();
    if(!device_ctx) {
        SAH_TRACEZ_WARNING("Device context for %s not found", PORTMAPPING_PATH);
        res = 0;
        goto exit;
    }
    amxd_object_for_each(instance, it_pf, pf_instances_obj) {
        pf_obj = amxc_container_of(it_pf, amxd_object_t, it);
        char* path = NULL;
        char* status = amxd_object_get_cstring_t(pf_obj, NC_PORTFORWARDING_STATUS, NULL);
        when_null_log(status, ctu_loop);
        if(strcmp(status, NC_PORTFORWARDING_STATUS_OFF) == 0) {
            goto ctu_loop;
        }
        path = amxd_object_get_cstring_t(pf_obj, NC_PORTFORWARDING_PATH, NULL);
        if(!string_is_empty(path)) {
            amxc_var_t ret;
            amxc_var_init(&ret);
            if(amxb_del(device_ctx, path, 0, NULL, &ret, 5) < 0) {
                SAH_TRACEZ_ERROR(ME, "Failed to remove portforwarding");
            }
            amxc_var_clean(&ret);
        }
        amxd_object_set_cstring_t(pf_obj,
                                  NC_PORTFORWARDING_STATUS,
                                  NC_PORTFORWARDING_STATUS_OFF);
        amxd_object_set_cstring_t(pf_obj, NC_PORTFORWARDING_PATH, "");
ctu_loop:
        free(status);
        free(path);
    }
    res = 0;
exit:
    return res;
}

/**
 * @brief Disable the firewall rules for a container
 *
 * @param ctr_obj
 * @return int
 */
static int networking_firewall_disable(amxd_object_t* ctr_obj) {
    int res = -1;
    amxd_object_t* rules_obj = NULL;
    amxd_object_t* rule_obj = NULL;
    amxd_object_t* config = NULL;
    char* firewall_chain_rules = NULL;
    amxb_bus_ctx_t* device_ctx = NULL;


    config = networking_get_plugin_settings(ctr_obj);
    when_null_log(config, exit);
    rules_obj = amxd_object_findf(config, NC_FIREWALLRULES);
    when_null_log(rules_obj, exit);
    firewall_chain_rules = networking_get_firewall_chain_rules();
    if(!firewall_chain_rules) {
        SAH_TRACEZ_INFO(ME, "DefaultFirewallChain is not defined");
        res = 0;
        goto exit;
    }
    device_ctx = amxb_be_who_has(firewall_chain_rules);
    if(!device_ctx) {
        SAH_TRACEZ_WARNING(ME, "Device context for %s not found", firewall_chain_rules);
        res = 0;
        goto exit;
    }

    amxd_object_for_each(instance, it_rules, rules_obj) {
        rule_obj = amxc_container_of(it_rules, amxd_object_t, it);
        char* path = amxd_object_get_cstring_t(rule_obj, NC_FIREWALLRULES_PATH, NULL);
        if(!string_is_empty(path)) {
            amxc_var_t ret;
            amxc_var_init(&ret);
            if(amxb_del(device_ctx, path, 0, NULL, &ret, 5) < 0) {
                SAH_TRACEZ_ERROR(ME, "Failed to remove firewall rule");
            }
            amxc_var_clean(&ret);
        }
        amxd_object_free(&rule_obj);
        free(path);
    }
    res = 0;
exit:
    free(firewall_chain_rules);
    return res;
}

/**
 * @brief Disable the DNS SD settings for a container
 *
 * @param ctr_obj
 * @return int
 */
static int networking_dnssd_disable(amxd_object_t* ctr_obj) {
    int res = -1;
    amxd_object_t* dnssd_instances_obj = NULL;
    amxd_object_t* dnssd_obj = NULL;
    amxd_object_t* config = NULL;
    amxb_bus_ctx_t* device_ctx_dnssd = NULL;
    amxb_bus_ctx_t* device_ctx_portmapping = NULL;

    config = networking_get_plugin_settings(ctr_obj);
    when_null_log(config, exit);
    dnssd_instances_obj = amxd_object_findf(config, NC_DNSSD);
    when_null_log(dnssd_instances_obj, exit);
    device_ctx_dnssd = networking_get_dnssd_bus_ctx();
    if(!device_ctx_dnssd) {
        SAH_TRACEZ_WARNING("Device context for %s not found", DNSSD_PATH);
        res = 0;
        goto exit;
    }
    device_ctx_portmapping = networking_get_portforwarding_bus_ctx();
    if(!device_ctx_portmapping) {
        SAH_TRACEZ_WARNING("Device context for %s not found", PORTMAPPING_PATH);
    }

    amxd_object_for_each(instance, it_dnssd, dnssd_instances_obj) {
        dnssd_obj = amxc_container_of(it_dnssd, amxd_object_t, it);
        char* path = NULL;
        char* pf_path = NULL;
        char* status = amxd_object_get_cstring_t(dnssd_obj, NC_DNSSD_STATUS, NULL);
        when_null_log(status, ctu_loop);
        if(strcmp(status, NC_DNSSD_STATUS_OFF) == 0) {
            goto ctu_loop;
        }
        path = amxd_object_get_cstring_t(dnssd_obj, NC_DNSSD_PATH, NULL);
        if(!string_is_empty(path)) {
            amxc_var_t ret;
            amxc_var_init(&ret);
            if(amxb_del(device_ctx_dnssd, path, 0, NULL, &ret, 5) < 0) {
                SAH_TRACEZ_ERROR(ME, "Failed to remove dnssd");
            }
            amxc_var_clean(&ret);
        }
        pf_path = amxd_object_get_cstring_t(dnssd_obj, NC_DNSSD_PORTFORWARDINGPATH, NULL);
        if(!string_is_empty(pf_path) && device_ctx_portmapping) {
            amxc_var_t ret;
            amxc_var_init(&ret);
            if(amxb_del(device_ctx_portmapping, pf_path, 0, NULL, &ret, 5) < 0) {
                SAH_TRACEZ_ERROR(ME, "Failed to remove portforwarding");
            }
            amxc_var_clean(&ret);
        }
        amxd_object_set_cstring_t(dnssd_obj,
                                  NC_PORTFORWARDING_STATUS,
                                  NC_PORTFORWARDING_STATUS_OFF);
        amxd_object_set_cstring_t(dnssd_obj, NC_DNSSD_PATH, "");
        amxd_object_set_cstring_t(dnssd_obj, NC_DNSSD_PORTFORWARDINGPATH, "");
ctu_loop:
        free(status);
        free(path);
        free(pf_path);
    }
    res = 0;
exit:
    return res;
}

/**
 * @brief Callback when an IP interface is added to a container
 *
 * this will apply portforwarding, firewall rules and DNSSD settings for a container
 *
 * @param sig_name
 * @param data
 * @param priv
 */
static void networking_itf_added(UNUSED const char* const sig_name,
                                 const amxc_var_t* const data,
                                 UNUSED void* const priv) {
    const char* ip_address = NULL;
    char* interface = NULL;
    const char* path = GET_CHAR(data, "path");
    if(string_is_empty(path)) {
        return;
    }
    amxc_var_t* parameters = GET_ARG(data, "parameters");
    when_null_log(parameters, exit);
    ip_address = GET_CHAR(parameters, CTHULHU_CONTAINER_INTERFACES_ADDRESSES_ADDRESS);
    when_str_empty_log(ip_address, exit);

    amxd_object_t* obj = amxd_dm_findf(dm, "%s", path);
    if(!obj) {
        SAH_TRACEZ_ERROR(ME, "Path not found");
        return;
    }

    amxd_object_t* itf_obj = amxd_object_findf(obj, "^.");
    interface = amxd_object_get_cstring_t(itf_obj, CTHULHU_CONTAINER_INTERFACES_NAME, NULL);
    if(strcmp(interface, "lo") == 0) {
        goto exit;
    }
    amxd_object_t* ctr_obj = amxd_object_findf(obj, "^.^.^.");

    networking_portforwarding_apply(ctr_obj, ip_address);
    networking_dnssd_apply(ctr_obj, ip_address);
    networking_firewall_apply(ctr_obj, ip_address);

exit:
    free(interface);
}

/**
 * @brief remove portforwarding settings that might still be present after a restart
 *
 * @return int
 */
static int networking_remove_lingering_portforwarding(void) {
    int res = -1;
    amxc_var_t values;
    amxc_var_init(&values);
    amxb_bus_ctx_t* device_ctx = networking_get_portforwarding_bus_ctx();
    if(!device_ctx) {
        SAH_TRACEZ_WARNING("Device context for %s not found", PORTMAPPING_PATH);
        res = 0;
        goto exit;
    }
    // Device.NAT.PortMapping.[Description == "Cthulhu"].?
    res = amxb_get(device_ctx, PORTMAPPING_PATH ".[ Description == '"CTHULHU "' ]", 0, &values, 5);
    if(res == AMXB_ERROR_BUS_NOT_FOUND) {
        res = 0;
        goto exit;
    } else if(res != 0) {
        SAH_TRACEZ_ERROR(ME, "amxb_get failed with error %d", res);
        res = -1;
        goto exit;
    }
    amxc_var_t* pf_list = amxc_var_get_first(&values);
    when_null(pf_list, exit);

    amxc_var_for_each(pf, pf_list) {
        const char* path = amxc_var_key(pf);
        if(amxb_del(device_ctx, path, 0, NULL, NULL, 5) < 0) {
            SAH_TRACEZ_ERROR(ME, "Failed to remove portforwarding");
        }
    }

    res = 0;
exit:
    amxc_var_clean(&values);
    return res;
}

/**
 * @brief remove firewall settings that might still be present after a restart
 *
 * @return int
 */
static int networking_remove_lingering_firewall_rules(void) {
    int res = -1;
    amxc_var_t values;
    char* firewall_chain_rules = NULL;
    amxc_string_t search_path;

    amxc_var_init(&values);
    amxc_string_init(&search_path, 64);

    firewall_chain_rules = networking_get_firewall_chain_rules();
    when_str_empty_log(firewall_chain_rules, exit);
    amxb_bus_ctx_t* device_ctx = amxb_be_who_has(firewall_chain_rules);
    if(!device_ctx) {
        SAH_TRACEZ_WARNING(ME, "Device context for %s not found", firewall_chain_rules);
        res = 0;
        goto exit;
    }     // Device.NAT.PortMapping.[Description == "Cthulhu"].?
    amxc_string_appendf(&search_path, "%s.[ Description == '"CTHULHU "' ]", firewall_chain_rules);
    res = amxb_get(device_ctx, search_path.buffer, 0, &values, 5);
    if(res == AMXB_ERROR_BUS_NOT_FOUND) {
        res = 0;
        goto exit;
    } else if(res != 0) {
        SAH_TRACEZ_ERROR(ME, "amxb_get failed with error %d", res);
        res = -1;
        goto exit;
    }
    amxc_var_t* fwrule_list = amxc_var_get_first(&values);
    when_null(fwrule_list, exit);

    amxc_var_for_each(fwrule, fwrule_list) {
        const char* path = amxc_var_key(fwrule);
        if(amxb_del(device_ctx, path, 0, NULL, NULL, 5) < 0) {
            SAH_TRACEZ_ERROR(ME, "Failed to remove portforwarding");
        }
    }

    res = 0;
exit:
    free(firewall_chain_rules);
    amxc_string_clean(&search_path);
    amxc_var_clean(&values);
    return res;
}

/**
 * @brief remove DNSSD settings that might still be present after a restart
 *
 * @return int
 */
static int networking_remove_lingering_dnssd(void) {
    int res = -1;
    amxc_var_t values;
    amxc_var_init(&values);
    amxb_bus_ctx_t* device_ctx = networking_get_dnssd_bus_ctx();
    if(!device_ctx) {
        SAH_TRACEZ_WARNING("Device context for %s not found", DNSSD_PATH);
        res = 0;
        goto exit;
    }
    // Device.DNS.SD.Advertise.[Alias starts with 'Cthulhu'].?
    res = amxb_get(device_ctx, DNSSD_PATH ".[ Alias starts with 'Cthulhu' ]", 0, &values, 5);
    if(res == AMXB_ERROR_BUS_NOT_FOUND) {
        res = 0;
        goto exit;
    } else if(res != 0) {
        SAH_TRACEZ_ERROR(ME, "amxb_get failed with error %d", res);
        res = -1;
        goto exit;
    }
    amxc_var_t* dnssd_list = amxc_var_get_first(&values);
    when_null(dnssd_list, exit);

    amxc_var_for_each(dnssd, dnssd_list) {
        const char* path = amxc_var_key(dnssd);
        if(amxb_del(device_ctx, path, 0, NULL, NULL, 5) < 0) {
            SAH_TRACEZ_ERROR(ME, "Failed to remove dnssd");
        }
    }

    res = 0;
exit:
    amxc_var_clean(&values);
    return res;
}

static int networking_init(UNUSED const char* function_name, UNUSED amxc_var_t* args, UNUSED amxc_var_t* ret) {
    int res = -1;
    amxc_var_t* var_dm = NULL;
    var_dm = GET_ARG(args, CTHULHU_PLUGIN_ARG_DATAMODEL);
    if(var_dm && var_dm->data.data) {
        dm = (amxd_dm_t*) var_dm->data.data;
    }

    amxb_bus_ctx_cthulhu = amxb_be_who_has(CTHULHU_DM);
    if(!amxb_bus_ctx_cthulhu) {
        SAH_TRACEZ_ERROR(ME, "Cannot get bus ctx for " CTHULHU_DM);
        goto exit;
    }

    if(amxb_subscribe(amxb_bus_ctx_cthulhu,
                      CTHULHU_DM_CONTAINER_INSTANCES ".*." CTHULHU_CONTAINER_INTERFACES ".*." CTHULHU_CONTAINER_INTERFACES_ADDRESSES ".",
                      "notification == 'dm:instance-added'",
                      networking_itf_added,
                      NULL) < 0) {
        SAH_TRACEZ_ERROR(ME, "Could not subscribe to IP interface events");
    }

    res = 0;
exit:
    return res;
}

static int networking_odl_loaded(UNUSED const char* function_name, UNUSED amxc_var_t* args, amxc_var_t* ret) {
    int res = 0;

    if(networking_remove_lingering_portforwarding() < 0) {
        SAH_TRACEZ_NOTICE(ME, "could not remove lingering portforwarding rules");
    }
    if(networking_remove_lingering_firewall_rules() < 0) {
        SAH_TRACEZ_NOTICE(ME, "could not remove lingering portforwarding rules");
    }
    if(networking_remove_lingering_dnssd() < 0) {
        SAH_TRACEZ_NOTICE(ME, "could not remove lingering dnssd rules");
    }


// exit:
    amxc_var_set(int32_t, ret, res);
    return res;
}

static int networking_cleanup(UNUSED const char* function_name, UNUSED amxc_var_t* args, amxc_var_t* ret) {
    int res = 0;

    amxc_var_set(int32_t, ret, res);
    return res;
}

static int register_function(const char* const func_name, amxm_callback_t cb) {
    int res = -1;
    if(amxm_module_add_function(mod, func_name, cb) < 0) {
        SAH_TRACEZ_ERROR(ME, "Could not add function [%s]", func_name);
        goto exit;
    }
    res = 0;
exit:
    return res;
}

/**
 * @brief check if the settings are valid
 *
 * the settings should contain a valid interface name, and the combination of access interfaces should be defined in the firewallrules
 *
 * @param access_itfs
 * @return int
 */
static bool networking_accessinterfaces_check_valid(const amxc_var_t* access_itfs) {
    bool res = false;
    amxc_llist_t ctr_access_intfs;
    amxc_llist_init(&ctr_access_intfs);

    amxc_var_for_each(itf, access_itfs) {
        const char* ref = GET_CHAR(itf, NC_ACCESSINTERFACES_REFERENCE);
        if(!ref) {
            SAH_TRACEZ_ERROR(ME, "The accessinterface has no "NC_ACCESSINTERFACES_REFERENCE " defined");
            goto exit;
        }
        amxc_llist_add_string(&ctr_access_intfs, ref);
    }
    amxd_object_t* rules = networking_get_firewall_rules(&ctr_access_intfs);
    if(!rules) {
        SAH_TRACEZ_ERROR(ME, "Access interfaces are not defined in the firewall rules");
        goto exit;
    }


    res = true;
exit:
    amxc_llist_clean(&ctr_access_intfs, amxc_string_list_it_free);
    return res;
}

/**
 * @brief add access interfaces parameters from InstallDU ot the datamodel
 *
 * @param access_itfs
 * @param dm_network_config
 * @return int
 */
static int networking_accessinterfaces_set_settings(const amxc_var_t* access_itfs, amxd_object_t* dm_network_config) {
    int res = -1;
    amxd_trans_t trans;
    amxd_status_t status;

    amxd_object_t* dm_accessinterfaces = amxd_object_findf(dm_network_config, NC_ACCESSINTERFACES);
    when_null_log(dm_accessinterfaces, exit);
    amxc_var_for_each(itf, access_itfs) {
        const char* ref = GET_CHAR(itf, NC_ACCESSINTERFACES_REFERENCE);
        ASSERT_NOT_NULL(ref, continue);

        status = amxd_status_ok;

        amxd_trans_init(&trans);
        amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);

        amxd_trans_select_object(&trans, dm_accessinterfaces);
        amxd_trans_add_inst(&trans, 0, NULL);

        amxd_trans_set_value(cstring_t, &trans, NC_ACCESSINTERFACES_REFERENCE, ref);
        status = amxd_trans_apply(&trans, dm);
        amxd_trans_clean(&trans);
        ASSERT_EQUAL(status, amxd_status_ok, continue);
    }
    res = 0;
exit:
    return res;
}

/**
 * @brief add portforwarding parameters using a transaction
 *
 * @param obj
 * @param interface
 * @param ext_port
 * @param int_port
 * @param protocol
 * @return amxd_status_t
 */
static amxd_status_t networking_portforwarding_settings_transaction(amxd_object_t* obj, const char* interface,
                                                                    uint16_t ext_port, uint16_t int_port,
                                                                    const char* protocol) {
    amxd_status_t status = amxd_status_ok;
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_select_object(&trans, obj);
    amxd_trans_add_inst(&trans, 0, NULL);

    amxd_trans_set_value(cstring_t, &trans, NC_PORTFORWARDING_INTERFACE, interface);
    amxd_trans_set_value(uint16_t, &trans, NC_PORTFORWARDING_EXTERNALPORT, ext_port);
    amxd_trans_set_value(uint16_t, &trans, NC_PORTFORWARDING_INTERNALPORT, int_port);
    amxd_trans_set_value(cstring_t, &trans, NC_PORTFORWARDING_PROTOCOL, protocol);
    status = amxd_trans_apply(&trans, dm);
    amxd_trans_clean(&trans);

    return status;
}

/**
 * @brief add portforwarding parameters from InstallDU ot the datamodel
 *
 * @param access_itfs
 * @param dm_network_config
 * @return int
 */
static int networking_portforwarding_set_settings(const amxc_var_t* portforwarding, amxd_object_t* dm_network_config) {
    int res = -1;
    amxd_status_t status;

    amxd_object_t* dm_portforwarding = amxd_object_findf(dm_network_config, NC_PORTFORWARDING);
    when_null_log(dm_portforwarding, exit);
    amxc_var_for_each(pf, portforwarding) {
        const char* interface = GET_CHAR(pf, NC_PORTFORWARDING_INTERFACE);
        ASSERT_NOT_NULL(interface, goto exit);
        uint16_t ext_port = GET_UINT16(pf, NC_PORTFORWARDING_EXTERNALPORT);
        ASSERT_NOT_EQUAL(ext_port, 0, goto exit);
        uint16_t int_port = GET_UINT16(pf, NC_PORTFORWARDING_INTERNALPORT);
        ASSERT_NOT_EQUAL(int_port, 0, goto exit);
        const char* protocol = GET_CHAR(pf, NC_PORTFORWARDING_PROTOCOL);
        ASSERT_NOT_NULL(protocol, goto exit);

        status = amxd_status_ok;

        if(strcmp(protocol, NC_PORTFORWARDING_PROTOCOL_BOTH) == 0) {
            status = networking_portforwarding_settings_transaction(dm_portforwarding, interface, ext_port,
                                                                    int_port, NC_PORTFORWARDING_PROTOCOL_TCP);
            status |= networking_portforwarding_settings_transaction(dm_portforwarding, interface, ext_port,
                                                                     int_port, NC_PORTFORWARDING_PROTOCOL_UDP);
        } else {
            status = networking_portforwarding_settings_transaction(dm_portforwarding, interface, ext_port,
                                                                    int_port, protocol);
        }

        ASSERT_EQUAL(status, amxd_status_ok, goto exit);
    }
    res = 0;
exit:
    return res;
}

/**
 * @brief save the DNSSD settings in the datamodel that are provided when a container is created
 *
 * @param dnssd
 * @param dm_network_config
 * @return int
 */
static int networking_dnssd_set_settings(const amxc_var_t* dnssd, UNUSED amxd_object_t* dm_network_config) {
    int res = -1;
    amxd_status_t status;
    amxd_trans_t trans;
    amxd_object_t* dm_textrecords = NULL;

    amxd_object_t* dm_dnssd = amxd_object_findf(dm_network_config, NC_DNSSD);
    ASSERT_NOT_NULL(dm_dnssd, goto exit);
    amxc_var_for_each(entry, dnssd) {
        bool enable = GET_BOOL(entry, NC_DNSSD_ENABLE);
        const char* interface = GET_CHAR(entry, NC_DNSSD_INTERFACE);
        ASSERT_NOT_NULL(interface, goto exit);
        const char* instance_name = GET_CHAR(entry, NC_DNSSD_INSTANCENAME);
        ASSERT_NOT_NULL(instance_name, goto exit);
        const char* application_protocol = GET_CHAR(entry, NC_DNSSD_APPLICATIONPROTOCOL);
        ASSERT_NOT_NULL(application_protocol, goto exit);
        const char* transport_protocol = GET_CHAR(entry, NC_DNSSD_TRANSPORTPROTOCOL);
        ASSERT_NOT_NULL(transport_protocol, goto exit);
        const char* domain = GET_CHAR(entry, NC_DNSSD_DOMAIN);
        ASSERT_NOT_NULL(domain, goto exit);
        uint16_t port = GET_UINT16(entry, NC_DNSSD_PORT);
        ASSERT_NOT_EQUAL(port, 0, goto exit);
        uint16_t internal_port = GET_UINT16(entry, NC_DNSSD_INTERNALPORT);
        ASSERT_NOT_EQUAL(port, 0, goto exit);

        status = amxd_status_ok;

        amxd_trans_init(&trans);
        amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
        amxd_trans_select_object(&trans, dm_dnssd);
        amxd_trans_add_inst(&trans, 0, NULL);

        amxd_trans_set_value(bool, &trans, NC_DNSSD_ENABLE, enable);
        amxd_trans_set_value(cstring_t, &trans, NC_DNSSD_INTERFACE, interface);
        amxd_trans_set_value(cstring_t, &trans, NC_DNSSD_INSTANCENAME, instance_name);
        amxd_trans_set_value(cstring_t, &trans, NC_DNSSD_APPLICATIONPROTOCOL, application_protocol);
        amxd_trans_set_value(cstring_t, &trans, NC_DNSSD_TRANSPORTPROTOCOL, transport_protocol);
        amxd_trans_set_value(cstring_t, &trans, NC_DNSSD_DOMAIN, domain);

        amxd_trans_set_value(uint16_t, &trans, NC_DNSSD_PORT, port);
        amxd_trans_set_value(uint16_t, &trans, NC_DNSSD_INTERNALPORT, internal_port);
        status = amxd_trans_apply(&trans, dm);

        amxd_trans_clean(&trans);
        ASSERT_EQUAL(status, amxd_status_ok, goto exit, "Failed to add a DNSSD entry");

        amxc_var_t* textrecords = GET_ARG(entry, NC_DNSSD_TEXTRECORD);
        if(textrecords) {
            dm_textrecords = amxd_object_findf(dm_dnssd,
                                               "[%s==%d && %s==\"%s\" && %s==\"%s\"].TextRecord", NC_DNSSD_PORT, port,
                                               NC_DNSSD_INTERFACE, interface, NC_DNSSD_TRANSPORTPROTOCOL, transport_protocol);

            ASSERT_NOT_NULL(dm_textrecords, goto exit_loop_failed);

            amxc_var_for_each(textrecord, textrecords) {
                amxd_trans_init(&trans);
                amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
                amxd_trans_select_object(&trans, dm_textrecords);

                amxd_trans_add_inst(&trans, 0, NULL);
                const char* key = GET_CHAR(textrecord, NC_DNSSD_TEXTRECORD_KEY);
                ASSERT_NOT_NULL(key, goto exit_loop_failed);
                const char* value = GET_CHAR(textrecord, NC_DNSSD_TEXTRECORD_VALUE);
                ASSERT_NOT_NULL(value, goto exit_loop_failed);

                amxd_trans_set_value(cstring_t, &trans, NC_DNSSD_TEXTRECORD_KEY, key);
                amxd_trans_set_value(cstring_t, &trans, NC_DNSSD_TEXTRECORD_VALUE, value);

                status = amxd_trans_apply(&trans, dm);
                amxd_trans_clean(&trans);
                ASSERT_EQUAL(status, amxd_status_ok, goto exit_loop_failed, "Failed to add a TXT record");

            }
        }
        continue;
exit_loop_failed:
        amxd_trans_init(&trans);
        amxd_trans_select_object(&trans, dm_dnssd);
        amxd_trans_add_inst(&trans, 0, NULL);
        amxd_trans_del_inst(&trans, amxd_object_get_index(dm_textrecords), NULL);
        amxd_trans_apply(&trans, dm);
        amxd_trans_clean(&trans);
        goto exit;
    }

    res = 0;
exit:
    return res;
}


/**
 * @brief Remove all the portforwarding settings for a container
 *
 * @param ctr_obj
 * @return int
 */
static int networking_remove_network_settings_for_ctr(amxd_object_t* ctr_obj) {
    int res = -1;
    amxd_object_t* config = NULL;

    config = networking_get_plugin_settings(ctr_obj);
    when_null_log(config, exit);
    {
        amxd_object_t* accessitf_instances_obj = amxd_object_findf(config, NC_ACCESSINTERFACES);
        when_null_log(accessitf_instances_obj, exit);
        amxd_object_for_each(instance, it_accessitf, accessitf_instances_obj) {
            amxd_object_t* obj = amxc_container_of(it_accessitf, amxd_object_t, it);
            amxd_object_free(&obj);
        }
    }

    {
        amxd_object_t* pf_instances_obj = amxd_object_findf(config, NC_PORTFORWARDING);
        when_null_log(pf_instances_obj, exit);

        amxd_object_for_each(instance, it_pf, pf_instances_obj) {
            amxd_object_t* obj = amxc_container_of(it_pf, amxd_object_t, it);
            amxd_object_free(&obj);
        }
    }

    {
        amxd_object_t* dnssd_instances_obj = amxd_object_findf(config, NC_DNSSD);
        when_null_log(dnssd_instances_obj, exit);

        amxd_object_for_each(instance, it_dnssd, dnssd_instances_obj) {
            amxd_object_t* obj = amxc_container_of(it_dnssd, amxd_object_t, it);
            amxd_object_free(&obj);
        }
    }

    res = 0;
exit:
    return res;
}

/**
 * @brief setup the network namespace of the private sandbox of the container, based on the AccessInterfaces
 *
 * @param ctr_obj
 * @return int
 */
static int networking_setup_sb_ns(amxd_object_t* ctr_obj) {
    int res = -1;
    char* bridge = NULL;
    char* default_itf_name = NULL;
    char* default_bridge = NULL;
    amxd_object_t* cthulhu = amxd_dm_findf(dm, CTHULHU);
    when_null_log(cthulhu, exit);
    amxd_object_t* plugin_settings = networking_get_plugin_settings(cthulhu);
    when_null_log(plugin_settings, exit);
    default_itf_name = amxd_object_get_cstring_t(plugin_settings, NC_DEFAULTINTERFACENAME, NULL);
    when_str_empty_log(default_itf_name, exit);
    default_bridge = amxd_object_get_cstring_t(plugin_settings, NC_DEFAULTBRIDGE, NULL);
    when_str_empty_log(default_bridge, exit);

    amxd_object_t* priv_sb_obj = networking_get_priv_sb(ctr_obj);
    when_null_log(priv_sb_obj, exit);
    networking_netns_configure_veth(priv_sb_obj, default_itf_name, default_bridge);
    res = 0;
exit:
    free(bridge);
    free(default_itf_name);
    free(default_bridge);
    return res;
}

static void networking_ctr_apply_networkconfig(amxd_object_t* ctr_obj, amxc_var_t* args) {
    amxc_var_t* var_data = NULL;
    networking_remove_network_settings_for_ctr(ctr_obj);
    amxd_object_t* dm_network_config = networking_get_plugin_settings(ctr_obj);
    when_null_log(dm_network_config, exit_empty);
    var_data = GET_ARG(args, CTHULHU_PLUGIN_ARG_CREATE_DATA);
    when_null_log(var_data, exit_empty);
    amxc_var_t* network_config = amxc_var_get_key(var_data, CTHULHU_CONTAINER_NETWORKCONFIG, AMXC_VAR_FLAG_DEFAULT);
    when_null(network_config, exit_empty);
    amxc_var_t* share_network = amxc_var_get_key(network_config, NC_SHAREPARENTNETWORK, AMXC_VAR_FLAG_DEFAULT);
    if(share_network) {
        if(amxc_var_get_bool(share_network)) {
            networking_netns_configure_parentNS(ctr_obj);

            amxd_status_t status = amxd_status_ok;
            amxd_trans_t trans;

            amxd_trans_init(&trans);
            amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
            amxd_trans_select_object(&trans, dm_network_config);
            amxd_trans_set_value(bool, &trans, NC_SHAREPARENTNETWORK, true);

            status = amxd_trans_apply(&trans, dm);
            amxd_trans_clean(&trans);
            ASSERT_EQUAL(status, amxd_status_ok, goto exit, "Failed to activate "NC_SHAREPARENTNETWORK);
            goto exit;
        }
    }
    amxc_var_t* access_itfs = amxc_var_get_key(network_config, NC_ACCESSINTERFACES, AMXC_VAR_FLAG_DEFAULT);
    if(access_itfs) {
        if(!networking_accessinterfaces_check_valid(access_itfs)) {
            SAH_TRACEZ_ERROR(ME, "Access interfaces are not valid");
            goto exit_empty;
        }
        networking_accessinterfaces_set_settings(access_itfs, dm_network_config);
    }

    amxc_var_t* portforwarding = amxc_var_get_key(network_config, NC_PORTFORWARDING, AMXC_VAR_FLAG_DEFAULT);
    if(portforwarding) {
        networking_portforwarding_set_settings(portforwarding, dm_network_config);
    }
    amxc_var_t* dnssd = amxc_var_get_key(network_config, NC_DNSSD, AMXC_VAR_FLAG_DEFAULT);
    if(dnssd) {
        networking_dnssd_set_settings(dnssd, dm_network_config);
    }

    networking_setup_sb_ns(ctr_obj);
    goto exit;
exit_empty:
    if(ctr_obj) {
        networking_netns_configure_empty(ctr_obj);
    }
exit:
    return;
}

static int networking_ctr_create(UNUSED const char* function_name, amxc_var_t* args, amxc_var_t* ret) {
    int res = 0;
    const char* ctr_id = GET_CHAR(args, CTHULHU_PLUGIN_ARG_CTR_ID);
    when_null_log(ctr_id, exit);
    amxd_object_t* ctr_obj = networking_ctr_get(ctr_id);
    when_null_log(ctr_obj, exit);

    networking_ctr_apply_networkconfig(ctr_obj, args);
exit:
    amxc_var_set(int32_t, ret, res);
    return res;
}

static int networking_ctr_modify(UNUSED const char* function_name, amxc_var_t* args, amxc_var_t* ret) {
    int res = 0;
    const char* ctr_id = GET_CHAR(args, CTHULHU_PLUGIN_ARG_CTR_ID);
    when_null_log(ctr_id, exit);
    amxd_object_t* ctr_obj = networking_ctr_get(ctr_id);
    when_null_log(ctr_obj, exit);

    networking_ctr_apply_networkconfig(ctr_obj, args);

exit:
    amxc_var_set(int32_t, ret, res);
    return res;
}

static int networking_ctr_poststop(UNUSED const char* function_name, UNUSED amxc_var_t* args, amxc_var_t* ret) {
    int res = 0;
    const char* ctr_id = GET_CHAR(args, CTHULHU_PLUGIN_ARG_CTR_ID);
    when_null_log(ctr_id, exit);
    amxd_object_t* ctr_obj = networking_ctr_get(ctr_id);
    when_null_log(ctr_obj, exit);
    if(networking_portforwarding_disable(ctr_obj) < 0) {
        SAH_TRACEZ_ERROR(ME, "CTR[%s]: Failed to disable port forwarding", ctr_id);
        res = -1;
    }
    if(networking_firewall_disable(ctr_obj) < 0) {
        SAH_TRACEZ_ERROR(ME, "CTR[%s]: Failed to disable firewall", ctr_id);
        res = -1;
    }
    if(networking_dnssd_disable(ctr_obj) < 0) {
        SAH_TRACEZ_ERROR(ME, "CTR[%s]: Failed to disable dnssd", ctr_id);
        res = -1;
    }
exit:
    amxc_var_set(int32_t, ret, res);
    return res;
}

AMXM_CONSTRUCTOR module_init(void) {
    SAH_TRACEZ_INFO(ME, ">>> Cthulhu Networking constructor\n");
    int ret = 1;

    so = amxm_so_get_current();
    if(so == NULL) {
        SAH_TRACEZ_INFO(ME, "Could not get shared object\n");
        goto exit;
    }

    if(amxm_module_register(&mod, so, CTHULHU_PLUGIN)) {
        SAH_TRACEZ_INFO(ME, "Could not register module %s\n", CTHULHU_PLUGIN);
        goto exit;
    }
    when_failed(register_function(CTHULHU_PLUGIN_INIT, networking_init), exit);
    when_failed(register_function(CTHULHU_PLUGIN_ODL_LOADED, networking_odl_loaded), exit);
    when_failed(register_function(CTHULHU_PLUGIN_CLEANUP, networking_cleanup), exit);
    when_failed(register_function(CTHULHU_PLUGIN_CTR_CREATE, networking_ctr_create), exit);
    when_failed(register_function(CTHULHU_PLUGIN_CTR_MODIFY, networking_ctr_modify), exit);
    when_failed(register_function(CTHULHU_PLUGIN_CTR_POSTSTOP, networking_ctr_poststop), exit);

    ret = 0;
exit:
    SAH_TRACEZ_INFO(ME, "<<< Cthulhu Networking constructor\n");
    return ret;
}

AMXM_DESTRUCTOR module_exit(void) {
    SAH_TRACEZ_INFO(ME, ">>> Cthulhu Networking destructor\n");
    SAH_TRACEZ_INFO(ME, "<<< Cthulhu Networking destructor\n");
    return 0;
}
